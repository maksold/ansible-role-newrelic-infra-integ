# Ansible Role: NewRelic Infrastructure Integrations

Configures NewRelic Infrastructure Integrations on RHEL/CentOS/Alma servers.

## Requirements

This role requires:

- Ansible 2.10+
- RHEL/CentOS/Alma 7/8/9

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

### ElasticSearch variables

    nrinfr_integ_elasticsearch_enabled: false
    nrinfr_integ_elasticsearch_package_state: "latest"
    nrinfr_integ_elasticsearch_command: all
    nrinfr_integ_elasticsearch_env: "staging"
    nrinfr_integ_elasticsearch_config_path: /etc/elasticsearch/elasticsearch.yml
    nrinfr_integ_elasticsearch_hostname: ""
    nrinfr_integ_elasticsearch_username: ""
    nrinfr_integ_elasticsearch_password: ""
    nrinfr_integ_elasticsearch_port: 9200
    nrinfr_integ_elasticsearch_timeout: 30
    nrinfr_integ_elasticsearch_role: elasticsearch

1. [GitHub repo for NewRelic integration for ElasticSearch](https://github.com/newrelic/nri-elasticsearch/)
1. [Configuration example "elasticsearch-config.yml.sample"](https://github.com/newrelic/nri-elasticsearch/blob/master/elasticsearch-config.yml.sample)
1. [NewRelic Docs for ElasticSearch monitoring integration](https://docs.newrelic.com/install/elastic/#instance-settings)


### Redis variables

    nrinfr_integ_redis_enabled: false
    nrinfr_integ_redis_package_state: "latest"
    nrinfr_integ_redis_env: "staging"
    nrinfr_integ_redis_hostname: "127.0.0.1"
    nrinfr_integ_redis_port: 6379
    nrinfr_integ_redis_password: ""
    nrinfr_integ_redis_remote_monitoring: true
    nrinfr_integ_redis_role: redis

1. [GitHub repo for NewRelic integration for Redis](https://github.com/newrelic/nri-redis/)
1. [Configuration example "redis-config.yml.sample"](https://github.com/newrelic/nri-redis/blob/master/redis-config.yml.sample)
1. [NewRelic Docs for Redis monitoring integration](https://docs.newrelic.com/docs/infrastructure/host-integrations/host-integrations-list/redis/redis-integration/#instance-settings)

### MySQL variables

    nrinfr_integ_mysql_enabled: false
    nrinfr_integ_mysql_package_state: "latest"
    nrinfr_integ_mysql_env: "staging"
    nrinfr_integ_mysql_hostname: "127.0.0.1"
    nrinfr_integ_mysql_port: 3306
    nrinfr_integ_mysql_user_state: "present"
    nrinfr_integ_mysql_username: "newrelic"
    nrinfr_integ_mysql_password: "xYuG&h?AdP$=?TTPnnmh&vkQ6Q=GhdUd"
    nrinfr_integ_mysql_password_encrypted: "*37E869825DCDD611E27FAC6657E06D84A8B69DAE"
    nrinfr_integ_mysql_database: ""
    nrinfr_integ_mysql_socket: "{{ mysql_socket | default('/var/lib/mysql/mysql.sock') }}"
    nrinfr_integ_mysql_extended_metrics: 1
    nrinfr_integ_mysql_extended_innodb_metrics: 1
    nrinfr_integ_mysql_extended_myisam_metrics: 1
    nrinfr_integ_mysql_remote_monitoring: true
    nrinfr_integ_mysql_role: mysql

1. [GitHub repo for NewRelic integration for MySQL](https://github.com/newrelic/nri-mysql)
1. [Configuration example "mysql-config.yml.sample"](https://github.com/newrelic/nri-mysql/blob/master/mysql-config.yml.sample)
1. [NewRelic Docs for MySQL monitoring integration](https://docs.newrelic.com/install/mysql/#config)


### Nginx variables

    nrinfr_integ_nginx_enabled: false
    nrinfr_integ_nginx_package_state: "latest"
    nrinfr_integ_nginx_env: "staging"
    nrinfr_integ_nginx_status_url: "http://127.0.0.1/nginx_status"
    nrinfr_integ_nginx_config_path: "/etc/nginx/nginx.conf"
    nrinfr_integ_nginx_remote_monitoring: true
    nrinfr_integ_nginx_role: nginx

1. [GitHub repo for NewRelic integration for Nginx](https://github.com/newrelic/nri-nginx)
1. [Configuration example "nginx-config.yml.sample"](https://github.com/newrelic/nri-nginx/blob/master/nginx-config.yml.sample)
1. [NewRelic Docs for Nginx monitoring integration](https://docs.newrelic.com/install/nginx/#instance-settings)

### Varnish variables

    nrinfr_integ_varnish_enabled: false
    nrinfr_integ_varnish_package_state: "latest"
    nrinfr_integ_varnish_env: "staging"
    nrinfr_integ_varnish_params_config_file: "/etc/varnish/varnish.params"
    nrinfr_integ_varnish_instance_name: "varnish-{{ nrinfr_integ_varnish_env }}"
    nrinfr_integ_varnish_role: varnish

1. [GitHub repo for NewRelic integration for Varnish](https://github.com/newrelic/nri-varnish/)
1. [Configuration example "varnish-config.yml.sample"](https://github.com/newrelic/nri-varnish/blob/master/varnish-config.yml.sample)
1. [NewRelic Docs for Varnish monitoring integration](https://docs.newrelic.com/docs/infrastructure/host-integrations/host-integrations-list/varnish-cache-monitoring-integration/)

### RabbitMQ variables

    nrinfr_integ_rabbitmq_enabled: false
    nrinfr_integ_rabbitmq_package_state: "latest"
    nrinfr_integ_rabbitmq_env: "staging"
    nrinfr_integ_rabbitmq_hostname: "127.0.0.1"
    nrinfr_integ_rabbitmq_port: 15672
    nrinfr_integ_rabbitmq_username: ""
    nrinfr_integ_rabbitmq_password: ""
    nrinfr_integ_rabbitmq_management_path_prefix: ""
    nrinfr_integ_rabbitmq_config_path: "/etc/rabbitmq/rabbitmq.conf"
    nrinfr_integ_rabbitmq_queues: ''
    nrinfr_integ_rabbitmq_role: rabbitmq

1. [GitHub repo for NewRelic integration for RabbitMQ](https://github.com/newrelic/nri-rabbitmq/)
1. [Configuration example "rabbitmq-config.yml.sample"](https://github.com/newrelic/nri-rabbitmq/blob/master/rabbitmq-config.yml.sample)
1. [NewRelic Docs for RabbitMQ monitoring integration](https://docs.newrelic.com/install/rabbitmq/#instance-settings)

## Useful commands

    sudo systemctl status newrelic-infra -l

Get status of NewRelic Infrastructure agent.

    ls -ltr /etc/newrelic-infra/integrations.d

Get list of config files from NewRelic Infrastructure integration directory.

## Dependencies

- [newrelic.newrelic-infra](https://github.com/newrelic/infrastructure-agent-ansible)

## License

MIT / BSD

## Author Information

This role was created in 2021 by [Maksim Soldatjonok](https://www.maksold.com/).